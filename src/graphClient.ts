import { Client, AuthenticationProvider } from '@microsoft/microsoft-graph-client';
import fetch from 'node-fetch';

import { config as readEnvVars } from 'dotenv';
readEnvVars();

const { SPO_TENANT_ID, AAD_APP_ID, AAD_APP_SECRET } = process.env;

export class AzureADAuthProvider implements AuthenticationProvider {
  public async getAccessToken(): Promise<string> {
    const { access_token } = await fetch(`https://login.microsoftonline.com/${SPO_TENANT_ID}/oauth2/v2.0/token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      },
      body: [
        `client_id=${AAD_APP_ID}`,
        `client_secret=${AAD_APP_SECRET}`,
        `grant_type=client_credentials`,
        `scope=https://graph.microsoft.com/.default`
      ].join('&')
    }).then(r => r.json());
    return access_token;
  }
}

export const graphClient = Client.initWithMiddleware({
  authProvider: new AzureADAuthProvider(),
});
