import { graph } from '@pnp/graph';
import { sp, Web, SPHttpClient } from '@pnp/sp';
import { AdalFetchClient, SPFetchClient } from '@pnp/nodejs';
import { PnpNode } from 'sp-pnp-node';
import { config as readEnvVars } from 'dotenv';
import { FetchOptions } from '@pnp/common';
// import { graphClient } from './graphClient';

// process.env.https_proxy = 'http://127.0.0.1:8888';
// process.env.http_proxy = 'http://127.0.0.1:8888';
// process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

readEnvVars();
const {
  SPO_TENANT, AAD_APP_ID, AAD_APP_SECRET,
  SPO_SITE_URL, SPO_CLIENT_ID, SPO_CLIENT_SECRET
} = process.env;

const graphFetch = new AdalFetchClient(SPO_TENANT, AAD_APP_ID, AAD_APP_SECRET);
graph.setup({
  graph: {
    fetchClientFactory: () => graphFetch
  }
});

// const spFetch = new SPFetchClient(SPO_SITE_URL, SPO_CLIENT_ID, SPO_CLIENT_SECRET);
const spFetch = new PnpNode({
  siteUrl: SPO_SITE_URL,
  authOptions: {
    clientId: SPO_CLIENT_ID,
    clientSecret: SPO_CLIENT_SECRET
  }
});
sp.setup({
  sp: {
    fetchClientFactory: () => spFetch
  }
});

(async () => {

  // const users = await graphClient.api('users').get();
  // console.log({ users });

  await graph.groups
    .select('id,mail').top(1).get()
    .then(g => console.log(`Groups: ${JSON.stringify(g, null, 4)}`));

  await new Web(SPO_SITE_URL).lists
    .select('Id,ListItemEntityTypeFullName').top(1).get()
    .then(l => console.log(`Lists: ${JSON.stringify(l, null, 4)}`));

  const graphClient = new SPHttpClient({
    fetch: (url: string, options: FetchOptions) => {
      return graphFetch.fetch(url, options);
    }
  });
  const users = await graphClient.get('https://graph.microsoft.com/v1.0/users').then(r => r.json());
  console.log({ users: JSON.stringify(users, null, 2) });

})()
  .catch(console.log);
