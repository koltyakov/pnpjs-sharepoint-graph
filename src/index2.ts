import { graphClient } from './graphClient';

(async () => {

  const users = await graphClient.api('users').get();
  console.log({ users });

})()
  .catch(console.log);
